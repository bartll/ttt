# Tic-Tac-Types für Liquid Haskell

Dieses Projekt ist eine Implementierung von [Tic-Tac-Types](https://doi.org/10.1145/3331554.3342606)
in [Liquid Haskell](https://ucsd-progsys.github.io/liquidhaskell-blog) im Rahmen meiner
Bachelorarbeit.


## Voraussetzungen

Zur Kompilierung werden [Stack](https://www.haskellstack.org/), [Git](https://git-scm.com/) und ein
SMT-Solver wie [Z3](https://github.com/Z3Prover/z3) benötigt.

Mithilfe von [Nix](https://nixos.org/nix) kann eine Shell mit allen benötigten Abhängigkeiten
geöffnet werden:

```sh
nix-shell
```
Für Nix mit [Flakes](https://nixos.wiki/wiki/Flakes) kann auch der folgende Befehl genutzt werden:

```sh
nix develop
```


## Kompilierung

Die Kompilierung des Projekts erfolgt mit Stack:

```sh
stack build
```
Auch eine interaktive Umgebung mit GHCi kann geöffnet werden:

```sh
stack ghci
```
