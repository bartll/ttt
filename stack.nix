{ nixpkgs
, pkgs ? import nixpkgs {}
}:

pkgs.symlinkJoin {
  inherit (pkgs.stack) name pname version;

  paths = [ pkgs.stack ];
  nativeBuildInputs = [ pkgs.makeWrapper ];
  postBuild =
    ''
      wrapProgram $out/bin/stack --add-flags '--nix --nix-path="nixpkgs=${nixpkgs}"'
    '';
}
