{ nixpkgs ? <nixpkgs>
, pkgs ? import nixpkgs {}
, ghc ? pkgs.haskell.compiler.ghc8107
}:

pkgs.haskell.lib.buildStackProject {
  inherit ghc;
  stack = import ./stack.nix { inherit nixpkgs pkgs; };

  name = "ttt";
  buildInputs = with pkgs; [ nix git z3 ];
}
