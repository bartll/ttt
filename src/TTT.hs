{-@ LIQUID "--exact-data-cons" @-}

module TTT where

-- Vorbereitungen

import Data.Either (isLeft)
import Text.Read (readMaybe)
import Data.Set (Set)
import qualified Data.Set as Set
import Language.Haskell.Liquid.ProofCombinators

{- `ListN` := Liste der Länge N
   `ListNE` := Liste, die nicht leer ist (NE = not empty)
 -}
{-@ type ListN a N = {v:[a] | len v == N} @-}
{-@ type ListNE a  = {v:[a] | len v > 0} @-}

{- `Pos` := positive Zahlen
   `Nat` := natürliche Zahlen (inklusive 0) (vordefiniert)
   `Fin` := natürliche Zahlen kleiner N
   `BNat` := natürliche Zahlen kleiner oder gleich N (vordefiniert)
 -}
{-@ type Pos   = {v:Int | v > 0} @-}
{-@ type Fin N = {v:Nat | v < N} @-}

-- `between lo x hi` :<=> `lo` <= `x` und `x` < `hi`
{-@ inline between @-} -- kann in der Refinement-Logik verwendet werden
between :: (Ord a) => a -> a -> a -> Bool
between lo x hi = lo <= x && x < hi

-- `between' lo hi x` :<=> `lo` <= `x` und `x` < `hi`
{-@ between' :: lo:a -> hi:a -> x:a -> {v:Bool | v <=> between lo x hi} @-}
between' lo hi x = between lo x hi

data Piece = X | O
  deriving (Show, Eq)

-- `swap X` := `O` und `swap O` := `X`
{-@ inline swap @-} -- kann in der Refinement-Logik verwendet werden
{-@ swap :: p:Piece -> {v:Piece | v /= p} @-}
swap X = O
swap O = X

{- Index
    Ein Index ist hier ein zum Indizieren geeigneter Datentyp.
    Dieser soll vergleichbar sein (also `Eq` implementieren).
    Außerdem soll man diesen in eine natürliche Zahl umwandeln können (`toNat`),
    die dann insbesondere zum Sortieren geeignet ist.
 -}

-- generisches Measure zum Umwandeln eines Index in eine natürliche Zahl
{-@ class measure toNat :: forall i. i -> Int @-}

{- Typklasse `Index`
    Diese stellt die Funktion `toNat` bereit, deren Ergebnis mit dem gleichnamigen Measure
    übereinstimmen muss.
 -}
{-@ class Index i where
      toNat :: x:i -> {v:Nat | v == toNat x}
  @-}
class Eq i => Index i where
    toNat :: i -> Int

-- `SIndex` (= Single Index), besteht nur aus einer natürlichen Zahl
{-@ data SIndex = SI Nat @-}
data SIndex = SI Int
    deriving (Show,Eq)

-- `toNat` ergibt für `SIndex` die natürliche Zahl
instance Index SIndex where
{-@ instance measure toNat :: SIndex -> Int
      toNat (SI n) = n
  @-}
    toNat (SI n) = n

{- `DIndex` (= Double Index)
    besteht aus einer Schranke `dibound` (positive Zahl, schränkt `disnd` ein)
    und aus zwei natürlichen Zahlen `difst` und `disnd`, wobei gilt:
    `disnd < dibound`
 -}
{-@ data DIndex = DI {dibound :: Pos, difst :: Nat, disnd :: Fin dibound} @-}
data DIndex = DI {dibound :: Int, difst :: Int, disnd :: Int}
    deriving (Show,Eq)

-- `DIndexN` := `DIndex` mit Schranke N
{-@ type DIndexN N = {v:DIndex | dibound v == N} @-}

-- `toNat` berechnet für `DIndex` den Term `dibound`*`difst` + `disnd`
instance Index DIndex where
{-@ instance measure toNat :: DIndex -> Int
      toNat (DI b x y) = b*x + y
  @-}
    toNat (DI b x y) = b*x + y

{- `dinext` berechnet den Nachfolger eines `DIndex`.
    Dabei gilt, dass `toNat` vom Nachfolger genau 1 mehr als `toNat` vom Parameter ergibt
 -}
{-@ dinext :: ix:DIndex -> {v: DIndexN {dibound ix} | toNat v == 1 + toNat ix} @-}
dinext (DI b x y)
  | y' == b   = DI b (x+1) 0
  | otherwise = DI b x     y'
  where
    y' = y + 1

-- IxList, Vector, Board

{- `IxList` (= indexed List)
    Eine indizierte Liste, die zu jedem Element auch dessen Index speichert.
    Dabei gilt, dass `toNat` vom Nachfolger eines Elements genau 1 mehr als `toNat` vom Index dieses
    Elements ergeben muss.
    Auch die leere Liste (`IxEmp`) hat einen Index, denn wenn diese ein Nachfolger eines Elements
    ist, muss auf deren Index zugegriffen werden.
 -}
{-@ data IxList [ixLen] i a where
      IxEmp  :: i -> IxList i a
      IxCons :: ix:i -> a -> {v: IxList i a | toNat (index v) == 1 + toNat ix} -> IxList i a
  @-}
data IxList i a = IxEmp i | IxCons i a (IxList i a)

{- `index` := Index des ersten Elements einer `IxList`
    measure: zum Verwenden in der Refinement-Logik (automatisches Verfeinern der Konstruktoren)
 -}
{-@ measure index @-}
index ::  IxList i a -> i
index (IxEmp  i)     = i
index (IxCons i _ _) = i

-- `ixLen` := Länge einer `IxList`
{-@ measure ixLen @-}
{-@ ixLen :: IxList i a -> Nat @-}
ixLen (IxEmp  _)     = 0 :: Int
ixLen (IxCons _ _ t) = 1 + ixLen t

{- `IxListN` := `IxList` der Länge N
   `IxListBiN` := `IxList` der Länge N mit Bi als Index des ersten Elements
   `IxListBN` := `IxList` der Länge N mit B als natürliche Zahl des Index des ersten Elements
   `IxListNE` := `IxList`, die nicht leer ist (NE = not empty)
 -}
{-@ type IxListN i a N = {v: IxList i a | ixLen v == N}
    type IxListBiN i a Bi N = {v: IxListN i a N | index v == Bi}
    type IxListBN i a B N = {v: IxListN i a N | toNat (index v) == B}
    type IxListNE i a = {v: IxList i a | ixLen v > 0}
  @-}

-- `ixHead` := Kopf einer `IxList`
{-@ measure ixHead @-}
{-@ ixHead :: IxListNE i a -> a @-}
ixHead (IxCons _ h _) = h

-- `ixTail` := Rest einer `IxList`
{-@ measure ixTail @-}
{-@ ixTail :: IxListNE i a -> IxList i a @-}
ixTail (IxCons _ _ t) = t

{- `ixElts` := Elemente einer `IxList`
    Diese werden als Menge (`Set`) von Paaren aus Index und Element repräsentiert
 -}
{-@ measure ixElts :: IxList i a -> Set (i,a)
      ixElts (IxEmp  _)     = Set.empty
      ixElts (IxCons i h t) = Set.union (Set.singleton (i,h)) (ixElts t)
  @-}

-- `ixSplitAt` := `IxList` bei Parameter `n` aufspalten und linken Teil zu normaler Liste umwandeln
{-@ ixSplitAt :: xs: IxList i a -> n: BNat {ixLen xs}
              -> (ListN a n, IxListBN i a {toNat (index xs) + n} {ixLen xs - n})
  @-}
ixSplitAt :: IxList i a -> Int -> ([a], IxList i a)
ixSplitAt xs             0 = ([],xs)
ixSplitAt (IxCons _ h t) n = (h:l1,l2)
  where
    (l1,l2) = ixSplitAt t (n-1)

-- `Vector` entspricht einer `IxList SIndex` beginnend beim Index 0 und mit Länge `vlen`
{-@ data Vector a = Vec {vlen :: Nat, siList :: IxListBN SIndex a 0 vlen} @-}
data Vector a = Vec {vlen :: Int, siList :: IxList SIndex a}

-- `VectorN` := `Vector` der Länge N
{-@ type VectorN a N = {v: Vector a | vlen v == N} @-}

-- `VIndex` := Index (`SIndex`) für `Vector` V
{-@ type VIndex V = {v:SIndex | between 0 (toNat v) (vlen V)} @-}

-- `vElts` := Elemente eines `Vector`s
{-@ measure vElts :: vec: Vector a -> Set (VIndex vec, a)
      vElts (Vec _ l) = ixElts l
  @-}

-- `makeVI` := `VIndex` konstruieren
{-@ makeVI :: vec: Vector a -> Fin {vlen vec} -> VIndex vec @-}
makeVI (Vec _ _) = SI

-- `fromList` := Liste in `Vector` umwandeln
{-@ fromList :: xs:[a] -> VectorN a {len xs} @-}
fromList xs = Vec l ys
  where
    (l,ys) = go xs 0 -- `l` = length, `ys` = `xs` as `IxList`
    {-@ go :: ys:[a] -> i:Nat
           -> ({v:Nat | v == i + (len ys)}, IxListBN SIndex a {i} {len ys})
      @-}
    go []    l = (l, IxEmp $ SI l)
    go (h:t) i = (l, IxCons (SI i) h ys)
      where
        (l,ys) = go t (i+1)

{- `Board` entspricht einem zweidimensionalem Feld der Größe `bsize`
    Es ist dabei als eindimensionale `IxList DIndex` codiert, beginnend bei Index 0 und mit
    Länge `bsize`*`bsize`, also Länge `bsize` für jede Dimension.
 -}
{-@ data Board a = Board {bsize :: Pos, diList :: IxListBN (DIndexN bsize) a 0 {bsize*bsize}} @-}
data Board a = Board {bsize :: Int, diList :: IxList DIndex a}

-- `BoardN` := Board der Größe N
{-@ type BoardN a N = {v: Board a | bsize v == N} @-}

-- `BIndex` := Index für `Board` B
{-@ type BIndex B = {v: DIndexN {bsize B} | between 0 (toNat v) ((bsize B)*(bsize B))} @-}

-- `bElts` := Elemente eines `Board`s
{-@ measure bElts :: b: Board a -> Set (BIndex b, a)
      bElts (Board _ l) = ixElts l
  @-}

-- `makeBI` := `BIndex` konstruieren
{-@ makeBI :: b: Board a -> Fin {bsize b} -> Fin {bsize b} -> BIndex b @-}
makeBI (Board n _) = DI n

-- `fill` := `Board` der Größe `n` konstruieren und mit `x` befüllen
{-@ fill :: n:Pos -> a -> BoardN a n @-}
fill n x = Board n $ go (DI n 0 0)
  where
    {-@ go :: i:{DIndexN n | toNat i <= n*n}
           -> IxListBiN (DIndexN n) a {i} {n*n - toNat i}
           / [n*n - toNat i]
      @-}
    go i | toNat i == n*n = IxEmp i
         | otherwise      = IxCons i x $ go (dinext i)

{- Verifizierte Zugriffsfunktionen / Type Safe Indexing
    Statt Linsen (lenses), die für das Typsystem zu allgemein wären, stellen die Funktionen
    `get` und `set` allgemeine Möglichkeiten dar, von einer `IxList` ein Element an einem bestimmten
    Index zu bekommen bzw. bei einer `IxList` einen Wert an einem bestimmten Index zu setzen.
    Die beiden Funktionen geben dabei Informationen über die Elemente der `IxList` zurück.
 -}

{- `get` := Element von einer `IxList` an einem bestimmten Index bekommen
    Der erste Parameter ist ein Beweis, dass `toNat` für diese Index-Instanz `i<p>` injektiv ist,
    denn sonst wäre die Funktion inkonsistent.
    Der zweite Parameter `ix` ist der Index, an dem das gesuchte Element ist.
    Der dritte Parameter `xs` ist die `IxList`, deren erster Index kleiner oder gleich `ix` sein
    muss und deren letzter Index (`toNat (index xs) + ixLen xs`) größer als `ix` sein muss.
    Als Ergebnis bekommt man ein Element mit der Garantie, dass dieses Element ein Element von `xs`
    am Index `ix` ist.
 -}
{-@ get :: forall i <p :: i -> Bool>.
           (j:i<p> -> k:i<p> -> {toNat j == toNat k => j == k})
        -> ix:i<p>
        -> xs:{IxList i<p> a | between (toNat (index xs)) (toNat ix) (toNat (index xs) + ixLen xs)}
        -> {v:a | Set.member (ix,v) (ixElts xs)}
  @-}
get :: (Index i) => (i -> i -> Proof) -> i -> IxList i a -> a
get p j (IxCons k h t)
  | j == k    = h
  | otherwise = get p j t ? proof
  where
    -- We need the proof that `toNat j` > `toNat k` in the `otherwise` case
    proof = p j k

{- `SRes` (= `set` result) := Garantie über das Ergebnis der Funktion `set`
    Dies besagt, dass der Wert X ein Element am Index I ist und dass die neuen Elemente E ohne X
    eine Teilmenge der alten Elemente F sind.
 -}
{-@ predicate SRes I X E F = Set.member (I,X) E
                             && Set.isSubsetOf (Set.difference E (Set.singleton (I,X))) F
  @-}

{- `set` := Wert an einem bestimmten Index in einer `IxList` setzen
    Der erste Parameter ist ein Beweis, dass `toNat` für diese Index-Instanz `i<p>` injektiv ist,
    denn sonst wäre die Funktion inkonsistent.
    Der zweite Parameter `ix` ist der Index, an dem der Wert gesetzt werden soll.
    Der dritte Parameter `x` ist der Wert, der gesetzt werden soll.
    Der vierte Parameter `xs` ist die `IxList`, deren erster Index kleiner oder gleich `ix` sein
    muss und deren letzter Index (`toNat (index xs) + ixLen xs`) größer als `ix` sein muss.
    Als Ergebnis bekommt man eine `IxList` mit der gleichen Größe und dem gleichen Index des ersten
    Elements wie `xs` und der Garantie `SRes`.
 -}
{-@ set :: forall i <p :: i -> Bool>.
           (j:i<p> -> k:i<p> -> {toNat j == toNat k => j == k})
        -> ix:i<p>
        -> x:a
        -> xs:{IxList i<p> a | between (toNat (index xs)) (toNat ix) (toNat (index xs) + ixLen xs)}
        -> {v: IxListBiN i<p> a {index xs} {ixLen xs} | SRes ix x (ixElts v) (ixElts xs)}
  @-}
set :: (Index i) => (i -> i -> Proof) -> i -> a -> IxList i a -> IxList i a
set p j x (IxCons k h t)
  | j == k    = IxCons k x t
  | otherwise = IxCons k h (set p j x t) ? proof
  where
    -- We need the proof that `toNat j` > `toNat k` in the `otherwise` case
    proof = p j k

-- `toNat_injective_SI` := Beweis, dass `toNat` für `SIndex` injektiv ist
{-@ toNat_injective_SI :: j:SIndex -> k:SIndex -> {toNat j == toNat k => j == k} @-}
toNat_injective_SI (SI _) (SI _) = trivial  -- proof by SMT

-- `getV` := `get` für `Vector`
{-@ getV :: vec: Vector a -> ix: VIndex vec -> {v:a | Set.member (ix,v) (vElts vec)} @-}
getV (Vec _ xs) ix = get toNat_injective_SI ix xs

-- `setV` := `set` für `Vector`
{-@ setV :: vec: Vector a -> ix: VIndex vec -> x:a
         -> {v: VectorN a {vlen vec} | SRes ix x (vElts v) (vElts vec)}
  @-}
setV (Vec n xs) ix x = Vec n $ set toNat_injective_SI ix x xs

-- `toNat_injective_DI` := Beweis, dass `toNat` für `DIndex` mit gleichem `dibound` injektiv ist
{-@ toNat_injective_DI :: j:DIndex -> k:DIndexN {dibound j} -> {toNat j == toNat k => j == k} @-}
toNat_injective_DI (DI _ _ _) (DI _ _ _) = trivial  -- proof by SMT

-- `getB` := `get` für `Board`
{-@ getB :: b: Board a -> ix: BIndex b -> {v:a | Set.member (ix,v) (bElts b)} @-}
getB (Board _ xs) ix = get toNat_injective_DI ix xs

-- `setB` := `set` für `Board`
{-@ setB :: b: Board a -> ix: BIndex b -> x:a
         -> {v: BoardN a {bsize b} | SRes ix x (bElts v) (bElts b)}
  @-}
setB (Board n xs) ix x = Board n $ set toNat_injective_DI ix x xs

-- Gewinnen / Winning

-- `rows` := Zeilen eines `Board`s
{-@ rows :: b: Board a -> ListN (ListN a {bsize b}) {bsize b} @-}
rows (Board n xs) = go n xs
  where
    {-@ go :: m:Nat -> xs: IxListN i a {n*m} -> ListN (ListN a n) m @-}
    go :: Int -> IxList i a -> [[a]]
    go 0 _  = []
    go m ys = l1 : go (m-1) l2
      where
        (l1,l2) = ixSplitAt ys n

{- `transpose` := transponieren einer zweidimensionelen Liste
    Hier ist ein zusätzlicher Parameter `m` nötig, um die Länge der inneren Listen und somit die
    Länge der Ausgabe-Liste zu spezifizieren.
 -}
{-@ transpose :: m:Nat -> xs:[ListN a m] -> ListN (ListN a {len xs}) m @-}
transpose :: Int -> [[a]] -> [[a]]
transpose 0 _  = []
transpose m xs = map head xs : transpose (m-1) (map tail xs)

-- `cols` := Spalten eines `Board`s
{-@ cols :: b: Board a -> ListN (ListN a {bsize b}) {bsize b} @-}
cols b = transpose (bsize b) (rows b)

-- `diags` := Diagonalen eines `Board`s
{-@ diags :: b: Board a -> ListN (ListN a {bsize b}) 2 @-}
diags b = [diag r, diag $ map reverse r]
  where
    r = rows b
    diag = go (bsize b)
    {-@ go :: n:Nat -> ListN (ListN a n) n -> ListN a n @-}
    go :: Int -> [[a]] -> [a]
    go 0 _           = []
    go n ((x:_):xss) = x : go (n-1) (map tail xss)

{- `possibles` := Mögliche Gewinnreihen (winning lines), also alle Zeilen, Spalten und Diagonalen.
    Das Measure führt eine uninterpretierte Funktion `possibles` in die Refinement-Logik ein, die
    dann später benutzt werden kann.  Wir nehmen an (`assume`), dass die `possibles`-Funktion genau
    dieser uninterpretierten Funktion entspricht und zusätzlich eine Liste der Länge
    2*(`bsize b`) + 2 für ein `Board` `b` berechnet.
 -}
{-@ measure possibles :: Board a -> [[a]] @-}

{-@ assume possibles :: b: Board a
                     -> {v: ListN (ListN a {bsize b}) {2*(bsize b) + 2} | v == possibles b}
  @-}
possibles b@(Board n _) = p
  where
    {-@ p :: ListN (ListN a n) {2*n + 2} @-} -- verify the length
    p = rows b ++ cols b ++ diags b

{- `Won` := Datentyp/Beweis, dass `wplayer` gewonnen hat.
    Hier geht es nicht nur um die Anzahl (`mostPieces`), sondern es wird eine Gewinnreihe `wline`
    verlangt, die nur aus `Just wplayer` besteht und die ein Element der möglichen Gewinnreihen
    `possibles wboard` ist.
 -}
{-@ data Won = HasWon {wplayer :: Piece
                      , wboard :: Board (Maybe Piece)
                      , wline  :: {v: ListN (Maybe Piece) {bsize wboard}
                                        | listElts v == Set.singleton (Just wplayer)
                                        && Set.member v (listElts (possibles wboard))}}
  @-}
data Won = HasWon {wplayer :: Piece, wboard :: Board (Maybe Piece), wline :: [Maybe Piece]}

-- `WonP` := Spieler P hat gewonnen
{-@ type WonP P = {v:Won | wplayer v == P} @-}

-- `allEqualTo` :<=> Liste `xs` besteht nur aus `x`
{-@ allEqualTo :: x:a -> xs: ListNE a -> {v:Bool | v <=> listElts xs == Set.singleton x} @-}
allEqualTo x [y]    = x == y
allEqualTo x (y:ys) = x == y && allEqualTo x ys

-- `won` := Überprüfen ob Spieler `p` auf Spielfeld `b` gewonnen hat und `Won` konstruieren
{-@ won :: p:Piece -> b: Board (Maybe Piece)
        -> Maybe ({v: WonP p | wboard v == b})
  @-}
won p b@(Board n _) = (HasWon p b) <$> find (possibles b)
  where
    -- `find` := `wline` finden
    {-@ find :: xs:[ListN (Maybe Piece) n]
             -> Maybe ({v: ListN (Maybe Piece) n | listElts v == Set.singleton (Just p)
                                                    && Set.member v (listElts xs)})
      @-}
    find []                   = Nothing
    find (h:t)
      | allEqualTo (Just p) h = Just h
      | otherwise             = find t

{- Verifizierte Spielzüge / I Like The Way You Move / Going Over The Top
    Statt der Lösung als Laufzeitüberprüfung wird hier sehr viel vom Typsystem überprüft.
    Statt dem GADT `TicTacTitanium` kommt allerdings ein normaler Haskell-ADT `TicTacToe`
    zum Einsatz mit Smart-Konstruktoren `start` und `turn`.
 -}

{- `TicTacToe` := TicTacToe-Spiel
    Dabei ist `tboard` das Spielfeld, `tplayer` der Spieler, der an der Reihe ist, und `tfree` die
    Anzahl der noch freien Felder.
    Um auf der sicheren Seite zu sein dürfen nur die Smart-Konstruktoren `start` und `turn`
    verwendet werden.
 -}
{-@ data TicTacToe [tfree] = TTT {tboard   :: Board (Maybe Piece)
                                 , tplayer :: Piece
                                 , tfree   :: BNat {(bsize tboard)*(bsize tboard)}}
  @-}
data TicTacToe = TTT {tboard :: Board (Maybe Piece), tplayer :: Piece, tfree :: Int}

-- `tsize` := Größe eines Spiels
{-@ inline tsize @-} -- kann in der Refinement-Logik verwendet werden
{-@ tsize :: TicTacToe -> Pos @-}
tsize t = bsize (tboard t)

-- `full` :<=> Spielfeld ist voll (keine freien Felder mehr)
{-@ inline full @-}
{-@ full :: t:TicTacToe -> {v:Bool | (v <=> tfree t == 0) && (not v <=> tfree t > 0)} @-}
full t = tfree t == 0

{- `TicTacToeP` := Spiel mit `tplayer` P
   `TicTacToeNPF` := Spiel der Größe N mit `tplayer` P und F freien Feldern
   `TicTacToeNF` := Spiel mit freien Feldern (NF = not full)
 -}
{-@ type TicTacToeP P = {v:TicTacToe | tplayer v == P} @-}
{-@ type TicTacToeNPF N P F = {v: TicTacToeP P | tsize v == N && tfree v == F} @-}
{-@ type TicTacToeNF = {v:TicTacToe | not (full v)} @-}

{- `StartRes` := Ergebnis des Smart-Konstruktors `start`
    Besteht aus einem Spiel, bei dem X an der Reihe ist und noch alle Felder frei sind.
    Außerdem ein Beweis, dass an jedem gültigen Index `Nothing` in `tboard` gespeichert ist.
 -}
{-@ data StartRes where
      SR :: g:{TicTacToeP {X} | tfree g == (tsize g)*(tsize g)}
         -> (ix: BIndex {tboard g} -> {Set.member (ix, Nothing) (bElts (tboard g))})
         -> StartRes
  @-}
data StartRes = SR TicTacToe (DIndex -> Proof)

-- `sgame` := TicTacToe-Spiel aus `StartRes` extrahieren
{-@ measure sgame @-}
sgame :: StartRes -> TicTacToe
sgame (SR g _) = g

-- Smart-Konstruktor `start`, konstruiert ein Spiel der Größe `n` (siehe `StartRes`)
{-@ start :: n:Pos -> {v:StartRes | tsize (sgame v) == n} @-}
start n = SR (TTT b X $ n*n) proof
  where
    b :: Board (Maybe Piece)
    b = fill n Nothing
    {-@ proof :: ix: BIndex b -> {Set.member (ix, Nothing) (bElts b)} @-}
    proof ix = getB b ix *** QED

{- `TurnRes` := Ergebnis des Smart-Konstruktors `turn`
    Besteht entweder aus einem `TurnWon` (Beweis, dass `tplayer` gewonnen hat) oder einem `TurnTTT`
    (Spiel, bei dem der andere Spieler an der Reihe ist und das ein freies Feld weniger hat).
    Außerdem wurden bei beiden Möglichkeiten ein `Just tplayer` auf dem Spielfeld hinzugefügt, dies
    wird durch `SRes` ausgedrückt.
 -}
{-@ type TurnWon G = {v: WonP {tplayer G} | bsize (wboard v) == tsize G} @-}
{-@ type TurnTTT G = TicTacToeNPF {tsize G} {swap (tplayer G)} {tfree G - 1} @-}
{-@ type TurnRes G M = Either ({v: TurnWon G | SRes M (Just (tplayer G))
                                                    (bElts (wboard v)) (bElts (tboard G))})
                              ({v: TurnTTT G | SRes M (Just (tplayer G))
                                                    (bElts (tboard v)) (bElts (tboard G))})
  @-}

{- Smart-Konstruktor `turn`
    Konstruiert ein `TurnRes` aus einem Spiel `game`.
    Dabei wird an der Stelle `move`, welche vorher frei sein muss, ein `Just tplayer` auf dem
    Spielfeld hinzugefügt und anschließend überprüft, ob `tplayer` gewonnen hat.
 -}
{-@ turn :: game:TicTacToeNF
         -> move:{BIndex {tboard game} | Set.member (move, Nothing) (bElts (tboard game))}
         -> TurnRes game move
  @-}
turn (TTT b p r) move = case won p b' of
                          Just w  -> Left w
                          Nothing -> Right $ TTT b' (swap p) (r-1)
  where
    b' = setB b move (Just p)

-- Ein- und Ausgaben / A Playable Game

{- `TTTEvent` := Fehlermeldungen beim Spielen
    Hierbei benötigen wir kein `GameWon`, da `turn` in diesem Fall ein `Won` zurückgibt.
 -}
data TTTEvent = PosTaken | BadString
  deriving (Show, Eq)

-- `TicTacEvent` entspricht `Maybe`, aber mit `TTTEvent`-Fehlermeldungen
type TicTacEvent = Either TTTEvent

{- `makeMove` := Überprüft mithilfe von `getB` ob die Vorbedingung von `turn` erfüllt ist und
    konstruiert dann mit `turn` ein `TurnRes` oder gibt `PosTaken` zurück.
    Entspricht `makeMoveTitanium`, prüft aber nicht auf `GameWon`, da `turn` das bereits erledigt
 -}
{-@ makeMove :: game:TicTacToeNF -> move: BIndex {tboard game}
             -> TicTacEvent (TurnRes game move)
  @-}
makeMove game move
  | posFree   = Right $ turn game move
  | otherwise = Left PosTaken
  where
    posFree = getB (tboard game) move == Nothing

-- `maybeToEither` := Convert `Just b` to `Right b` and `Nothing` to `Left a`
{-@ maybeToEither :: a -> m: Maybe b -> {v: Either a b | isLeft v <=> not (isJust m)} @-}
maybeToEither a (Just b) = Right b
maybeToEither a Nothing  = Left a

{- `toMaybe` := `filter` für `Maybe`
    Nimmt ein Prädikat und einen Wert `x` und gibt `Just x` zurück, wenn der Wert das Prädikat
    erfüllt und sonst `Nothing`.
    In der Refinement Logik führen wir ebenfalls ein Prädikat `p` ein und spezifizieren mittels
    Bounded Refinements und `q`, dass `p` gelten soll, wenn `x` das übergebene Prädikat erfüllt.
    Damit können wir ein `Maybe a<p>` zurückgeben.
 -}
{-@ toMaybe :: forall a <p :: a -> Bool, q :: a -> Bool -> Bool>.
               {x :: a, b :: {v:Bool<q x> | v} |- {v:a | v == x} <: a<p>}
               (x:a -> Bool<q x>) -> a -> Maybe a<p>
  @-}
toMaybe p x
  | p x       = Just x
  | otherwise = Nothing

-- `toMove` := versuchen einen Index für das Spielfeld zu parsen (`BadString` falls Fehler)
{-@ toMove :: b: Board a -> String -> TicTacEvent (BIndex b) @-}
toMove b s = maybeToEither BadString $ do
    xs <- traverse readMaybe $ words s  -- try to parse integers from `s`
    ms <- traverse toFin xs             -- convert `Int` to `Fin`
    case ms of
      [x,y] -> Just $ makeBI b x y      -- exactly two `Fin`s -> success
      _     -> Nothing                  -- other value -> failure
  where
    -- use `toMaybe` to get a `Fin {bsize b}` from any `Int`
    toFin = toMaybe (between' 0 $ bsize b)

-- `prettyBoard` := ein Spielfeld schön formatieren
prettyBoard :: Board (Maybe Piece) -> String
prettyBoard = unlines . map (map p2c) . rows
  where
    p2c Nothing  = ' '
    p2c (Just X) = 'X'
    p2c (Just O) = 'O'

{- `runGame` := Spiel ausführen
    Der Parameter bestimmt die Spielfeldgröße.
    Nach jedem Spielzug wird das Spielfeld ausgegeben.
 -}
{-@ runGame :: Pos -> IO () @-}
runGame = play . sgame . start
  where
    printBoard  = putStr . prettyBoard
    printWon w  = putStrLn $ "Player " ++ show (wplayer w)
                                       ++ " has won"
    play :: TicTacToe -> IO ()
    play game
      | full game = putStrLn "Draw: Board is full"
      | otherwise = do
          {-@ readMove :: IO (TicTacEvent (BIndex {tboard game})) @-}
          let readMove = toMove (tboard game) <$> getLine
          {-@ makeM :: TicTacEvent (BIndex {tboard game})
                    -> TicTacEvent (Either (TurnWon game) (TurnTTT game))
            @-}
          let makeM = (>>= makeMove game)
          putStr "Move> "
          move <- readMove
          case makeM move of
            Left  e         -> print e  -- Error
            Right (Left  w) -> do       -- Won
                printBoard (wboard w)
                printWon w
            Right (Right g) -> do       -- Continue
                printBoard (tboard g)
                play g

-- `runGame3` := Spiel ausführen für das bekannte 3×3-TicTacToe
runGame3 :: IO ()
runGame3 = runGame 3
