{
  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixpkgs-unstable;
  inputs.flake-utils.url = github:numtide/flake-utils;

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system: {
      devShell =
        let
          pkgs = import nixpkgs { inherit system; };
          stack = import ./stack.nix { inherit nixpkgs pkgs; };
        in
          pkgs.mkShell {
            nativeBuildInputs = [ stack pkgs.nix ];
          };
    });
}
